<!DOCTYPE HTML>
<html lang="en-US">
<head>

	<!--Coded by Mutiullah Samim-->
	<meta charset="UTF-8">
	<title>Products</title>
	
	<!--Bootsrap CDN-->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	
	
	
   <!--Style-->	
	<style>
	 #deleteCheckbox{
		 margin-left:5px;
		 margin-top:5px;
		 height:20px;
		 width:20px;
	 }
	
	</style>
	
	
   <!--Including database connection file-->	
   <?php
   include('db_con.php');
    ?>
</head>
<body>

	
	<div class="container mt-5">
	
	  <div class="row">
	    <div class="col-md-7 col-sm-6 mb-2">
			<a href="add_product.html">
			<button class="btn btn-success">Add Product</button>
			</a>
	    </div>
	 
	    <div class="col-md-5 col-sm-6 mb-2">
		   <form method="POST" class="float-right" action="index.php">
			 <div class="form-group float-left mx-2" >
				 <select class="form-control" required>
					<option disabled selected value="">Choose an action</option>
					<option value="delete_action">Mass Delete Action</option>
				 </select>
			 </div>
			 <button type="submit" class="btn btn-info" name="delete_btn">Apply</button>
	    </div>
		
	  </div>
	  <hr>
	
	
	<!--Displaying all the products in database-->
	  <?php
	
	$products = "SELECT * FROM products";
    $result = $con->query($products);
    echo '<div class="row">';
    if ($result->num_rows > 0) {
		 
    while($product = $result->fetch_assoc()) {
    echo '<div class="card my-4 mx-2" style="width: 16rem;">';
	?><input name="delete[]" type="checkbox" id="deleteCheckbox" value="<?php echo $product["id"]?>"/>
	<?php
    echo '<div class="card-body text-center">';
    echo '<h5 class="card-title">';
	echo $product["sku"];
	echo '</h5>';
    echo '<h6 class="card-text">';
	echo $product["name"];
	echo '</h6>';
    echo '<h6 class="card-text">';
	echo number_format($product["price"],2);
	echo ' $
	     </h6>';
		 //Check product type
		 if($product["type"]=='dvd'){
		 echo '<h6 class="card-text">';
		 echo 'Size: ';
         echo $product["size"];	
         echo ' MB
		 </h6>';		 
		 }
		 
		 //Check product type
		 if($product["type"]=='book'){
		 echo '<h6 class="card-text">';
		 echo 'Weight: ';
         echo $product["weight"];	
         echo ' KG
		 </h6>';		 
		 }
		 //Check product type
		 if($product["type"]=='furniture'){
		 echo '<h6 class="card-text">';
		 echo 'Dimension: ';
         echo $product["height"];	
         echo ' X ';
		 echo $product["width"];
		 echo ' X ';
		 echo $product["lenght"];
		 echo '</h6>';		 
		 }
		 
    echo '</div>
          </div>';

        }
        } 
	 
	else {
    echo "No products available";
        }
  
	
	
		  
	
		  
	 echo "</div>";
	
	  ?>
		</form>
	</div>
	
	
	
	<!--Deleting multiple products-->
	<?php
	
	   if(isset($_POST['delete_btn'])){
		   
		 //Getting all checked products ID  
	     $select=$_POST['delete'];
		 
	     //Checking if any product checkbox is ckecked
	     if (count($_POST["delete"]) > 0 ) {
			 
			$allchecked = implode(",", $_POST["delete"]);
			//Delete checked product if it is exist in database
			$query="DELETE FROM products WHERE 1 AND id IN($allchecked)";
		    
			//Check if deleting was successful
			if(mysqli_query($con,$query)){
				header("Location:index.php");
		  
          
			}
		}
	  
	  

		}
		   
	
	?>
	


	<!--Bootsrap CDN-->
	
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>